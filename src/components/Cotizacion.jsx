import styled from '@emotion/styled';
import PropTypes from 'prop-types';

const Div = styled.div`
  color: #FFFFFF;
  font-family: Arial, Helvetica, sans-serif;
`;
const Precio = styled.p`
  font-size: 30px;
`;
const Info = styled.p`
  font-size: 18px;
`;


const Cotizacion = ({ cotizacion }) => {

  if (Object.keys(cotizacion).length === 0) return null;

  const { PRICE, HIGHDAY, LOWDAY, CHANGEPCT24HOUR, LASTUPDATE } = cotizacion;

  return (
    <Div>
      <Precio><b>El precio es: </b>{ PRICE }</Precio>
      <Info><b>El precio mas alto del dia: </b>{ HIGHDAY }</Info>
      <Info><b>El precio mas bajo del dia: </b>{ LOWDAY }</Info>
      <Info><b>Cambio ultimas 24 horas: </b>{ CHANGEPCT24HOUR }%</Info>
      <Info><b>Ultima actualización: </b>{ LASTUPDATE }</Info>
    </Div>
  );
};

Cotizacion.propTypes = {
  cotizacion: PropTypes.object.isRequired
};

export default Cotizacion;
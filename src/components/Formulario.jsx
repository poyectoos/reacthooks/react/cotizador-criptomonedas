import { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import axios from 'axios';

import useMoneda from '../hooks/useMoneda';
import useCriptomoneda from '../hooks/useCriptomoneda';

import Error from './Error';

const Button = styled.button`
  margin-top: 20px;
  font-weight: bold;
  font-size: 20px;
  padding: 10px;
  background-color: #66A2FE;
  border: none;
  width: 100%;
  border-radius: 5px;
  color: #FFFFFF;
  transition: background-color .2s ease;

  &:hover {
    background-color: #326AC0;
    cursor: pointer;
  }
`;

const Formulario = ({ actualizarMoneda, actualizarCriptomoneda }) => {

  // Monedas
  const MONEDAS = [
    {
      codigo: 'USD',
      nombre: 'Dolar americano'
    },
    {
      codigo: 'MXN',
      nombre: 'Peso mexicano'
    },
    {
      codigo: 'EUR',
      nombre: 'Euro'
    },
    {
      codigo: 'GBP',
      nombre: 'Libra esterlina'
    }
  ];

  // State para las criptomonedas
  const [ data, actualizarData ] = useState([]);
  // State para el error
  const [ error, actualizarError ] = useState(false);

  // Utilizamos useMoney
  const [ moneda, SelectMonedas ] = useMoneda('Elige tu moneda', '', MONEDAS);

  const [ criptomoneda, SelectCriptomoneda ] = useCriptomoneda('Selecciona una criptomoneda', '', data);

  useEffect(() => {
    const consultar = async () => {
      const URI = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;

      const data = await axios.get(URI);

      actualizarData(data.data.Data);
    }
    consultar();
  }, []);

  const handleSubmit = e => {
    e.preventDefault();

    if (moneda === '' || criptomoneda === '') {
      actualizarError(true);
      return;
    }
    actualizarError(false);

    actualizarMoneda(moneda);
    actualizarCriptomoneda(criptomoneda);
  }

  return (
    <Fragment>
      {
        error
          ?
        <Error mensaje="Todos los campos son obligatorios" />
          :
        null
      }
      <form onSubmit={ handleSubmit }>
        <SelectMonedas />
        <SelectCriptomoneda />
        <Button type="submit">Calcular</Button>
      </form>
    </Fragment>
  );
};

Formulario.propTypes = {
  actualizarMoneda: PropTypes.func.isRequired,
  actualizarCriptomoneda: PropTypes.func.isRequired
};

export default Formulario;
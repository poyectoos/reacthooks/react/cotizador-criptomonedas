import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const Alerta = styled.p`
  background-color: rgb(197, 29, 52);
  padding: 1rem;
  color: #FFFFFF;
  font-size: 20px;
  text-transform: uppercase;
  text-align: center;
  font-family: 'Bebas Neue', cursive;
`;

const Error = ({ mensaje }) => {
  return (
    <Alerta>
      <b>Ups!!!</b> { mensaje }
    </Alerta>
  );
};

Error.propTypes = {
  mensaje: PropTypes.string.isRequired
};

export default Error;
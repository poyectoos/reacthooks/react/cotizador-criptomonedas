import { Fragment, useState } from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
  font-family: 'Bebas Neue', cursive;
  color: #FFFFFF;
  text-transform: uppercase;
  font-size: 2rem;
  font-weight: bold;
  display: block;
  margin-top: 1rem;
`;
const Select = styled.select`
  width: 100%;
  display: block;
  padding: 0.6rem;
  -webkit-appearance: none;
  border: none;
  font-size: 1.2rem;
`;

const useCriptomoneda = (label, stateInicial, opciones) => {

  const [ state, updateState ] = useState(stateInicial);

  const handleChange = e => {
    updateState(e.target.value);
  }

  const SelectCriptomoneda = () => (
    <Fragment>
      <Label htmlFor="criptomoneda">{ label }</Label>
      <Select
        name="criptomoneda"
        value={ state }
        onChange={ handleChange }
      >
        <option value="">Selecciona una criptomoneda</option>
        {
          opciones.map(opcion => (
            <option
              key={ opcion.CoinInfo.Id }
              value={ opcion.CoinInfo.Name }
            >
              { opcion.CoinInfo.FullName }
            </option>
          ))
        }
      </Select>
    </Fragment>
  );

  return [ state, SelectCriptomoneda, updateState ];
};

export default useCriptomoneda;
import { Fragment, useState } from 'react';
import styled from '@emotion/styled';

const Label = styled.label`
  font-family: 'Bebas Neue', cursive;
  color: #FFFFFF;
  text-transform: uppercase;
  font-size: 2rem;
  font-weight: bold;
  display: block;
  margin-top: 1rem;
`;
const Select = styled.select`
  width: 100%;
  display: block;
  padding: 0.6rem;
  -webkit-appearance: none;
  border: none;
  font-size: 1.2rem;
`;

const useMoneda = (label, stateInicial, opciones) => {

  const [ state, updateState ] = useState(stateInicial);

  const handleChange = e => {
    updateState(e.target.value);
  }

  const Seleccionar = () => (
    <Fragment>
      <Label htmlFor="moneda">{ label }</Label>
      <Select
        name="moneda"
        id="moneda"
        value={ state }
        onChange={ handleChange }
      >
        <option value="" disabled>Selecciona una moneda</option>
        {
          opciones.map(opcion => (
            <option
              key={ opcion.codigo }
              value={ opcion.codigo }
            >
              { opcion.nombre }
            </option>
          ))
        }
      </Select>
    </Fragment>
  );

  // Retornar state, interfaz y fn que modifica el state
  return [state, Seleccionar, updateState];
}

export default useMoneda;